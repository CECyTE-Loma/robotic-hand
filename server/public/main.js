const sockets = io();

sockets.on('data', function (data) {
  console.log('data ', data);
});


// setTimeout(() => {
//   sockets.emit('muestra-frase', 'Hola mundo');
// }, 3000);
document.getElementById('enviarFrase').onsubmit = function(e) {
  e.preventDefault();
  console.log('Enviar datos');
  var frase = document.getElementById('frase').value;
  console.log('frase ', frase);
  sockets.emit('muestra-frase', frase);
};

document.getElementById('holaComunidad').onclick = function (e) {
  sockets.emit('muestra-frase', 'Hola comunidad CECyTE');
};
document.getElementById('gracias').onclick = function (e) {
  sockets.emit('muestra-frase', 'gracias');
};
document.getElementById('adios').onclick = function (e) {
  sockets.emit('muestra-frase', 'adios');
};
