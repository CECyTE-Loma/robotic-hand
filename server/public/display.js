const sockets = io();
var frase = '';
sockets.on('data', function(data) {
  data = data.trim();
  if (data === 'inicia-frase') {
    document.getElementById('letra').innerText = '';
    document.getElementById('frase').innerText = '';
    frase = '';
    return;
  }
  if (data === 'YA TERMINEEEÉ') {
    document.getElementById('letra').innerText = '';
    frase = '';
    return;
  }
  frase += data === '' ? ' ' : data;
  document.getElementById('letra').innerText = data === '' ? '__' : data;
  document.getElementById('frase').innerText = frase;

  console.log('data ', data);
});
