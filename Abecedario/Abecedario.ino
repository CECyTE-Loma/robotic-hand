// Incluímos la librería para poder controlar los servos
#include <Servo.h>

// Declaramos las variables para controlar los servos
Servo dedoMenique;
Servo dedoAnular;
Servo dedoMedio;
Servo dedoIndice;
Servo dedoPulgar;
Servo basePulgar;
Servo munecaX;
Servo munecaY;
String frase = "Hola cecyte";
String incoming = "";
void setup() {
  // Iniciamos el monitor serie para mostrar el resultado
  Serial.begin(9600);

  dedoIndice.attach(2);
  dedoMenique.attach(3);
  dedoAnular.attach(4);
  dedoMedio.attach(5);
  dedoPulgar.attach(6);
  munecaX.attach(7);
  munecaY.attach(8);
  basePulgar.attach(9);
  posicionInicial();
  // Iniciamos los servos para que empiecen a trabajar con sus respectivos pin
}

void loop() {
  if (Serial.available() > 0) {
    // read the incoming:
    frase = Serial.readString();

    Serial.println("inicia-frase");
    // Serial.println(frase);
    delay(1000);
    mostrarFrase();
    Serial.flush();
  }
}

void mostrarFrase() {
  int cnt = frase.length();
  frase.toUpperCase();
  for (int x = 0; x < cnt; x++) {
    
    char l = frase[x];
 
    switch (l) {
    case 'A':
      letraA();
      break;
    case 'B':
      letraB();
      break;
    case 'C':
      letraC();
      break;
    case 'D':
      letraD();
      break;
    case 'E':
      letraE();
      break;
    case 'F':
      letraF();
      break;
    case 'G':
      letraG();
      break;
    case 'H':
      letraH();
      break;
    case 'I':
      letraI();
      break;
    case 'J':
      letraJ();
      break;
    case 'K':
      letraK();
      break;
    case 'L':
      if (x < cnt - 1 && frase[x + 1] == 'L') {
        x++;
        // Serial.println("LL");
        letraLL();
      }
      else {
        // Serial.println(l);
        letraL();
      }
      break;
    case 'M':
      letraM();
      break;
    case 'N':
      letraN();
      break;
    case '1':
      letraNN();
      break;
    case 'O':
      letraO();
      break;
    case 'P':
      letraP();
      break;
    case 'Q':
      letraQ();
      break;
    case 'R':
      if (x < cnt - 1 && frase[x + 1] == 'R') {
        x++;
        letraRR();
      }
      else {
        letraR();
      }

      break;
    case 'S':
      letraS();
      break;
    case 'T':
      letraT();
      break;
    case 'U':
      letraU();
      break;
    case 'V':
      letraV();
      break;
    case 'W':
      letraW();
      break;
    case 'X':
      letraX();
      break;
    case 'Y':
      letraY();
      break;
    case 'Z':
      letraZ();
      break;
    case ' ':
      espacio();
      break;
    }
  }
  Serial.print("YA TERMINEEEÉ");
}

void posicionInicial() {

  dedoIndice.write(0);
  dedoMenique.write(180);
  dedoAnular.write(0);
  dedoMedio.write(180);
  dedoPulgar.write(0);
  munecaX.write(40);  // 0 derecha 90 izquierda centro 40
  munecaY.write(100); // 100 arriba 25 abajo
  basePulgar.write(120);
}

void letraA() {

  posicionInicial();

  delay(1000);
  Serial.println("A");

  dedoMedio.write(180);
  dedoIndice.write(150);
  dedoAnular.write(180);
  dedoMedio.write(0);
  dedoMenique.write(50);
  dedoPulgar.write(80);
  delay(1500);
  posicionInicial();
}

void letraB() {

  posicionInicial();
  delay(1000);
  Serial.println("B");
  dedoPulgar.write(90);
  basePulgar.write(40);
  delay(1500);
  posicionInicial();
}

void letraC() {

  posicionInicial();
  delay(1000);
  Serial.println("C");
  dedoIndice.write(90);
  dedoMedio.write(65);
  dedoAnular.write(100);
  dedoMenique.write(120);
  basePulgar.write(45);
  delay(500);
  munecaX.write(0);
  delay(1500);
  posicionInicial();
}

void letraD() {

  posicionInicial();
  delay(1000);
  Serial.println("D");
  dedoMedio.write(180);
  //dedoIndice.write(150);
  dedoAnular.write(180);
  dedoMedio.write(0);
  dedoMenique.write(50);
  dedoPulgar.write(50);
  basePulgar.write(25);
  delay(1500);
  posicionInicial();
}

void letraE() {

  posicionInicial();
  delay(1000);
  Serial.println("E");
  dedoMedio.write(180);
  dedoIndice.write(150);
  dedoAnular.write(180);
  dedoMedio.write(20);
  dedoMenique.write(70);
  dedoPulgar.write(100);
  basePulgar.write(15);
  delay(1500);
  posicionInicial();
}

void letraF() {
  posicionInicial();
  delay(1000);
  Serial.println("F");
  //dedoMedio.write(180);
  dedoIndice.write(150);

  dedoPulgar.write(100);
  basePulgar.write(100);
  delay(1500);
  posicionInicial();
}

void letraG() {

  posicionInicial();
  delay(1000);
  Serial.println("G");

  //dedoIndice.write(150);
  dedoAnular.write(170);
  dedoMedio.write(0);
  dedoMenique.write(50);
  dedoPulgar.write(50);
  basePulgar.write(25);
  delay(1500);
  munecaX.write(0);
  munecaY.write(25);
  delay(2000);
  posicionInicial();
}

void letraH() {

  posicionInicial();
  delay(1000);
  Serial.println("H");
  //dedoIndice.write(150);
  dedoAnular.write(180);
  dedoMenique.write(50);
  dedoPulgar.write(90);
  basePulgar.write(25);
  delay(1000);
  munecaX.write(90);
  munecaY.write(25);
  delay(750);
  posicionInicial();
  delay(1500);
}

void letraI() {

  posicionInicial();
  delay(1000);
  Serial.println("I");
  dedoMedio.write(180);
  dedoIndice.write(150);
  dedoAnular.write(180);
  dedoMedio.write(0);
  //dedoMenique.write(50);
  dedoPulgar.write(100);
  delay(1500);
  posicionInicial();
}
void letraJ() {

  posicionInicial();
  delay(1000);
  Serial.println("J");
  dedoMedio.write(180);
  dedoIndice.write(150);
  dedoAnular.write(180);
  dedoMedio.write(0);
  dedoPulgar.write(100);
  munecaX.write(0);
  delay(500);
  munecaX.write(90);
  munecaY.write(30);
  delay(500);
  munecaY.write(100);
  delay(2000);
  posicionInicial();
}

void letraK() {

  posicionInicial();
  delay(1000);
  Serial.println("K");
  dedoMedio.write(70);
  dedoAnular.write(180);
  dedoMenique.write(50);
  dedoPulgar.write(100);

  delay(750);
  munecaY.write(30);
  delay(750);
  munecaY.write(100);
  delay(750);
  munecaY.write(30);
  delay(750);
  munecaY.write(100);
  posicionInicial();
}

void letraL() {

  posicionInicial();
  delay(1000);
  Serial.println("L");
  dedoAnular.write(180);
  dedoMedio.write(0);
  dedoMenique.write(50);
  delay(1300);
  posicionInicial();
}

void letraLL() {

  posicionInicial();
  delay(1000);
  Serial.println("LL");
  dedoAnular.write(180);
  dedoMedio.write(0);
  dedoMenique.write(50);
  delay(500);
  munecaX.write(0);
  delay(750);
  munecaX.write(90);
  delay(750);
  munecaX.write(0);
  delay(750);
  munecaX.write(90);
  delay(750);
  munecaX.write(40);
  delay(1500);
  posicionInicial();
}

void letraM() {
  posicionInicial();
  delay(1000);
  Serial.println("M");
  dedoIndice.write(100);
  dedoMedio.write(50);
  dedoAnular.write(110);
  //dedoMenique.write(30);
  dedoPulgar.write(100);
  delay(500);
  munecaY.write(30);
  delay(1500);
  posicionInicial();
}

void letraN() {

  posicionInicial();
  delay(1000);
  Serial.println("N");
  dedoIndice.write(100);
  dedoMedio.write(50);
  //dedoAnular.write(0);
  //dedoMenique.write(30);
  dedoPulgar.write(100);
  delay(500);
  munecaY.write(30);
  delay(1500);
  posicionInicial();
}

void letraNN() {
  posicionInicial();
  delay(1000);
  Serial.println("Ñ");
  dedoIndice.write(100);
  dedoMedio.write(50);
  //dedoAnular.write(0);
  //dedoMenique.write(30);
  dedoPulgar.write(100);
  delay(500);
  munecaY.write(30);
  delay(500);
  munecaX.write(20);
  delay(750);
  munecaX.write(60);
  delay(750);
  munecaX.write(40);
  delay(1000);
  posicionInicial();
}
void letraO() {
  posicionInicial();
  delay(1000);
  Serial.println("O");
  dedoIndice.write(130);
  dedoMedio.write(40);
  dedoAnular.write(110);
  dedoMenique.write(80);
  dedoPulgar.write(50);
  basePulgar.write(35);

  delay(1500);
  posicionInicial();
}

void letraP() {

  posicionInicial();
  delay(1000);
  Serial.println("P");
  dedoMedio.write(70);
  dedoAnular.write(180);
  dedoMenique.write(50);
  dedoPulgar.write(100);
  delay(1500);

  posicionInicial();
}

void letraQ() {

  posicionInicial();
  delay(1000);
  Serial.println("Q");
  dedoIndice.write(100);
  dedoMedio.write(0);
  dedoAnular.write(180);
  dedoMenique.write(40);
  //dedoPulgar.write(50);
  basePulgar.write(45);
  delay(500);
  munecaY.write(30);
  delay(750);
  munecaX.write(0);
  delay(1500);
  posicionInicial();
}

void letraR() {

  posicionInicial();
  delay(1000);
  Serial.println("R");
  dedoIndice.write(60);
  dedoMedio.write(140);
  dedoAnular.write(180);
  dedoMenique.write(50);
  dedoPulgar.write(130);

  delay(1500);
  posicionInicial();
}
void letraRR() {

  posicionInicial();
  delay(1000);
  Serial.println("RR");
  dedoIndice.write(60);
  dedoMedio.write(140);
  dedoAnular.write(180);
  dedoMenique.write(50);
  dedoPulgar.write(130);
  delay(500);
  munecaX.write(0);
  delay(750);
  munecaX.write(90);
  delay(750);
  munecaX.write(0);
  delay(750);
  munecaX.write(90);
  delay(750);
  munecaX.write(40);
  posicionInicial();
}

void letraS() {
  posicionInicial();
  delay(1000);
  Serial.println("S");
  dedoIndice.write(150);
  dedoMedio.write(0);
  dedoAnular.write(180);
  dedoMenique.write(50);
  basePulgar.write(10);
  delay(500);
  dedoPulgar.write(150);
  delay(500);
  munecaX.write(20);
  delay(750);
  munecaX.write(70);
  delay(750);
  munecaX.write(20);
  delay(750);
  munecaX.write(70);
  delay(750);
  munecaX.write(40);
  delay(1500);
  posicionInicial();
}

void letraT() {

  posicionInicial();
  delay(1000);
  Serial.println("T");
  dedoIndice.write(180);
  dedoMedio.write(0);
  //dedoAnular.write(180);
  dedoMenique.write(50);
  basePulgar.write(10);
  // delay(500);
  // dedoPulgar.write(150);
  // delay(500);
  // munecaX.write(20);
  // delay(750);
  // munecaX.write(70);
  // delay(750);
  // munecaX.write(20);
  // delay(750);
  // munecaX.write(70);
  // delay(750);
  // munecaX.write(40);
  delay(1500);
  posicionInicial();
}

void letraU() {
  posicionInicial();
  delay(1000);
  Serial.println("U");
  dedoAnular.write(180);
  dedoMenique.write(50);
  dedoPulgar.write(80);
  //delay(150);
  basePulgar.write(25);
  delay(750);
  munecaY.write(125);
  delay(1500);
  posicionInicial();
}

void letraV() {
  posicionInicial();
  delay(1000);
  Serial.println("V");
  dedoAnular.write(180);
  dedoMenique.write(50);
  dedoPulgar.write(80);
  //delay(150);
  basePulgar.write(25);
  delay(1500);
  posicionInicial();
}

void letraW() {

  posicionInicial();
  delay(1000);
  Serial.println("W");
  //dedoAnular.write(180);
  dedoMenique.write(50);
  dedoPulgar.write(80);
  //delay(150);
  basePulgar.write(25);
  delay(1500);
  posicionInicial();
}
void letraX() {
  posicionInicial();
  delay(1000);
  Serial.println("X");
  dedoIndice.write(90);
  dedoMedio.write(0);
  dedoAnular.write(180);
  dedoMenique.write(50);
  dedoPulgar.write(70);
  //delay(150);
  basePulgar.write(25);
  delay(750);
  munecaY.write(65);

  delay(500);
  munecaX.write(20);
  delay(750);
  munecaX.write(70);
  delay(750);
  munecaX.write(20);
  delay(750);
  munecaX.write(70);
  delay(750);
  munecaX.write(40);
  delay(1500);
  posicionInicial();
}

void letraY() {

  posicionInicial();
  delay(1000);
  Serial.println("Y");
  dedoIndice.write(150);
  dedoMedio.write(180);
  dedoAnular.write(180);
  dedoMedio.write(0);
  delay(1500);
  posicionInicial();
}
void letraZ() {

  posicionInicial();
  delay(1000);
  Serial.println("Z");
  dedoIndice.write(150);
  dedoMedio.write(180);
  dedoAnular.write(180);
  dedoMedio.write(0);
  //dedoMenique.write(50);
  dedoPulgar.write(90);
  basePulgar.write(15);
  delay(500);
  munecaX.write(90);
  munecaY.write(60);
  delay(800);
  munecaX.write(20);
  delay(800);
  munecaX.write(70);
  munecaY.write(30);
  delay(800);
  munecaX.write(20);
  delay(1500);
  posicionInicial();
}

void espacio() {
  posicionInicial();
  Serial.println("");
  delay(2000);
}